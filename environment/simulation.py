from actors.voiture import Voiture
from pygame.event import Event
from pygame.time import set_timer


class Simulator:
    def __init__(self, level_map, actors_surfaces, actors_event_code):
        self.map = level_map
        self.cars_location = dict()
        self.cars_out = list()
        self.cars_destroyed = list()
        self.actors_events = dict()

        self.__initiate_actors__(actors_surfaces, actors_event_code)

    def __initiate_actors__(self, actors_surfaces, actors_event_code):

        actor_code = actors_event_code
        car_millis = 200
        # Initiating cars objects
        for start_tile in self.map.cars_start_points:
            # Creating car object
            new_car = Voiture()
            new_car.direction = start_tile.directions[0]
            new_car.surface = actors_surfaces["car"]

            # Adding the car to the tile
            start_tile.occupants.append(new_car)
            self.cars_location[new_car] = start_tile

            # Initiating events
            car_event = Event(actor_code, actor=new_car, action=self.move_car)

            self.actors_events[new_car] = car_event
            set_timer(car_event, car_millis, loops=1)
            actor_code += 1
            car_millis += 200

    def move_car(self, car):
        """
        Determine a car's new position as per it's methods results
        :param car: the car object to be moved
        :return: None
        """
        tile = self.cars_location[car]
        set_timer(self.actors_events[car], 800, loops=1) # Setting default timer for next action

        if car.tourner_droite(tile):
            car.direction = "NESO"[("NESO".index(car.direction) + 1) % 4]
            self.map.redraw_tile(tile)

        if car.tourner_gauche(tile):
            car.direction = "NOSE"[("NOSE".index(car.direction) + 1) % 4]
            self.map.redraw_tile(tile)

        if car.avancer(tile):
            next_tile = tile.tuiles_voisines[car.direction]
            self.map.remove_actor_from_tile(car, tile)

            if next_tile.type != "Fin":
                # Moving car to next tile
                self.map.add_actor_to_tile(car, next_tile)

                if len(next_tile.occupants) > 1:
                    print("ACCIDENT")
                    self.cars_destroyed.append(car)
                    # Stopping other actors timers in tile
                    for actor in next_tile.occupants:
                        set_timer(self.actors_events[actor], 0)  # Stopping car timer

                elif next_tile.type in ("Herbe",):
                    print("SORTIE DE ROUTE !")
                    self.cars_out.append(car)
                    set_timer(self.actors_events[car], 0)  # Stopping car timer
                else:
                    self.cars_location[car] = next_tile
            else:
                # Car has reached an end tile
                del self.cars_location[car]
                set_timer(self.actors_events[car], 0)  # Stopping car timer

    def move_actors(self):
        # Moving cars
        pending_cars = list(self.cars_location.keys())
        for car in pending_cars:
            self.move_car(car)

    def move_actor(self, actor):
        self.move_car(actor)


