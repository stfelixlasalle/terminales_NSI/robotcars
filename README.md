Version Française disponible dans : LISEZMOI.md

# RobotCars

This is a Python Game which creates a situation for student to train on OOP at :
- writing methods
- adding props
- editing constructor
- accessing other object's props and methods

The game's inspiration is traffic control : the player acts on the cars by modifying the Car class so all cars on the board are able to get out the screen without accident.