from environment.tiles import Tuile


class Voiture:
    def __init__(self):
        self.direction = "N"  # La direction de la voiture est corrigée lors de son instanciation sur sa tuile de départ

    def tourner_gauche(self, position: Tuile) -> bool:
        """
        Le jeu interroge la voiture pour déterminer si elle souhaite tourner à gauche.
        :param position : Objet Tuile sur laquelle se trouve la voiture.
        :return: Vrai ou Faux selon qu'il faille tourner à gauche ou pas.
        """
        # Votre code...
        return False

    def tourner_droite(self, position: Tuile) -> bool:
        """
        Le jeu interroge la voiture pour déterminer si elle souhaite tourner à droite.
        :param position : Objet Tuile sur laquelle se trouve la voiture.
        :return: Vrai ou Faux selon qu'il faille tourner à droite ou pas.
        """
        # Votre code...
        return False

    def avancer(self, position: Tuile) -> bool:
        """
        Le jeu interroge la voiture pour déterminer si elle souhaite avancer.
        :param position : Objet Tuile sur laquelle se trouve la voiture.
        :return: Vrai ou Faux selon qu'il faille avancer ou pas.
        """
        # Votre code...
        return False
